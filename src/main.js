let logTextArea = document.getElementById("logTextArea");
let outputTextArea = document.getElementById("outputTextArea");
let apiKeyInput = document.getElementById("apiKeyInput");
let channelIdInput = document.getElementById("channelIdInput");
let downloadButton = document.getElementById("downloadButton");

function getCookie(name) {
  var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
  if (match) return match[2];
  return "";
}

function log(msg) {
  logTextArea.value += msg + "\n";
  logTextArea.scrollTop = logTextArea.scrollHeight;
}

function output(metadata) {
  console.log(metadata);
  if (typeof metadata === "object" && metadata.length > 0) {
    outputTextArea.value = "publishedAt;viewCount;likeCount;title;description;tags;link\n";
    metadata.map((row) => {
      outputTextArea.value += row.join(";").replace(/(?:\r\n|\r|\n)/g, "\\n") + "\n";
    });

    downloadButton.removeAttribute("disabled");
  }
}

function download() {
  let filename = "ytcme.csv";
  var file = new Blob([outputTextArea.value], {type: "text/csv"});
  if (window.navigator.msSaveOrOpenBlob) // IE10+
    window.navigator.msSaveOrOpenBlob(file, filename);
  else { // Others
    var a = document.createElement("a"),
        url = URL.createObjectURL(file);
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    setTimeout(function() {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 0);
  }
}

function getVideoMetadata(apiKey, idList, pageNumber, metadata) {
  if (pageNumber === undefined) pageNumber = 1;
  if (idList === undefined) return;
  if (metadata === undefined) metadata = [];

  let idSlice = idList.slice(0, 50);
  let apiUrl = `https://www.googleapis.com/youtube/v3/videos?key=${apiKey}&id=${idSlice.join(',')}&maxResults=50&part=id,snippet,statistics`;

  let request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4) {
      let response = JSON.parse(this.responseText);
      if (this.status == 200) {
        log(`    Fetched page ${pageNumber} of video api`);
        let videos = [];
        response.items.map((item) => {
          videos.push([
            item.snippet.publishedAt,
            item.statistics.viewCount,
            item.statistics.likeCount,
            `"${item.snippet.title}"`,
            `"${item.snippet.description}"`,
            `"${item.snippet.tags !== undefined ? item.snippet.tags.join(",") : ""}"`,
            `"https://www.youtube.com/watch?v=${item.id}"`,
          ]);
        });
        log(`    Found info for ${videos.length} videos on page ${pageNumber}`);
        metadata = metadata.concat(videos);

        let newIdList = idList.slice(50);
        if (newIdList.length > 0) {
          console.log(newIdList);
          getVideoMetadata(apiKey, newIdList, pageNumber+1, metadata);
        } else {
          log(`    Found ${metadata.length} video IDs in total`);
          output(metadata);
        }
      } else {
        log("Error calling search api");
        if (response !== undefined) log(`    Error code ${response.error.code}: ${response.error.message}`);
      }
    }
  };

  request.open("GET", apiUrl, true);
  request.send();
}

function getVideoIds(apiKey, channelId, pageToken, idList, pageNumber) {
  if (pageNumber === undefined) pageNumber = 1;
  if (idList === undefined) idList = [];
  let apiUrl = `https://www.googleapis.com/youtube/v3/search?key=${apiKey}&channelId=${channelId}&maxResults=50&part=id&order=date`;
  if (typeof pageToken === "string" && pageToken !== "") apiUrl += `&pageToken=${pageToken}`;

  let request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4) {
      let response = JSON.parse(this.responseText);
      if (this.status == 200) {
        log(`    Fetched page ${pageNumber} of search api`);
        let pageIds = [];
        response.items.map((item) => {
          if (item.id.kind === "youtube#video") {
            pageIds.push(item.id.videoId);
          }
        });
        log(`    Found ${pageIds.length} video IDs on page ${pageNumber}`);
        idList = idList.concat(pageIds);

        if (response.nextPageToken !== undefined) {
          getVideoIds(apiKey, channelId, response.nextPageToken, idList, pageNumber+1);
        } else {
          log(`    Found ${idList.length} video IDs in total`);
          log("Tabulating video info");
          getVideoMetadata(apiKey, idList);
        }
      } else {
        log("Error calling search api");
        if (response !== undefined) log(`    Error code ${response.error.code}: ${response.error.message}`);
      }
    }
  };

  request.open("GET", apiUrl, true);
  request.send();
}

function fetchMetadata(e) {
  let apiKey = apiKeyInput.value;
  let channelId = channelIdInput.value;
  if (apiKey !== "" && channelId !== "") {
    document.cookie = "apiKey=" + apiKey;
    document.cookie = "channelId=" + channelId;
  } else {
    log("Enter API Key and Channel ID to continue");
    return;
  }

  log("Tabulating list of video ids");
  getVideoIds(apiKey, channelId);

  return;
}


logTextArea.value = "";
apiKeyInput.value = getCookie("apiKey");
channelIdInput.value = getCookie("channelId");
if (outputTextArea.value.length > 0) {
  downloadButton.removeAttribute("disabled");
} else {
  downloadButton.setAttribute("disabled", "");
}
